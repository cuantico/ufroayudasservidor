package cl.ufro.bd;

import java.io.Serializable;

import utils.ObjetoBd;


public class Muro extends ObjetoBd implements Serializable {
	private int idAyudantia;
	private int idMsj;
	private String fecha;
	private String correo;	
	private String mensaje;
	private boolean confirmacion;
	public Muro(){
		addToPrimaryKey("idMsj");
	}
	public int getIdAyudantia() {
		return idAyudantia;
	}
	public void setIdAyudantia(int id) {
		this.idAyudantia = id;
	}
	public int getIdMsj() {
		return idMsj;
	}
	public void setIdMsj(int idMsj) {
		this.idMsj = idMsj;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public boolean isConfirmacion() {
		return confirmacion;
	}
	public void setConfirmacion(boolean confirmacion) {
		this.confirmacion = confirmacion;
	}

	
}
