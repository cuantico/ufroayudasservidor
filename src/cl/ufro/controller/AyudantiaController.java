package cl.ufro.controller;

import java.io.ObjectOutputStream;
import java.net.Socket;

import cl.ufro.bd.Ayudantia;
import cl.ufro.bd.Usuario;
import cl.ufro.service.AyudantiaService;
import cl.ufro.solicitud.RegistrarAyudantia;
import cl.ufro.solicitud.RegistrarUsuario;

public class AyudantiaController {

	private AyudantiaService ayudantiaService = new AyudantiaService();
	
	public Ayudantia findById(Ayudantia ayudantia){
		return ayudantiaService.findById(ayudantia);
	}
	public void actualizar(Ayudantia ayudantia) {
		ayudantiaService.actualizar(ayudantia);
	}
	public Ayudantia guardar(Ayudantia ayudantia){
		return ayudantiaService.guardar(ayudantia);
	}
	
}
