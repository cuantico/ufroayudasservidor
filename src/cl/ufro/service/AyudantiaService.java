package cl.ufro.service;

import cl.ufro.bd.Ayudantia;
import cl.ufro.bd.Usuario;
import cl.ufro.dao.AyudantiaDao;

public class AyudantiaService {
	private AyudantiaDao ayudantiaDao = new AyudantiaDao();
	
	/*REVISAR SU USO EN EL PROGRAMA
	 * Funcion identificar:
	 * Recibe una ayudantia por parametros,se llama al Dao,una ayudantiaDao es retornado para comparar
	 * si el id registrado coincide con el id que es de interes.
	 * retorna el la ayudantia registrada.(nulo en caso de que no coincida el id).
	 * 
	 * */
	public Ayudantia identificar(Ayudantia ayudantia){
		Ayudantia ayudantiaBD = ayudantiaDao.findById(ayudantia); //BUSCAMOS POR ID
		if(ayudantiaBD!=null && ayudantiaBD.getId()==ayudantia.getId())
			return ayudantiaBD;
		return null;		
	}
	/*Funcion actualizar:
	 * Accede al Dao para actualizar los campos de la ayudantia.
	 * 
	 * */
	
	public void actualizar(Ayudantia ayudantia) {
		ayudantiaDao.update(ayudantia);
	}
	/*Funcion findById:
	 * Accede al dao y retorna la ayudantia que coincida con el id de la ayudantia que 
	 * se recibe por parametros.
	 * 
	 * 
	 * */
	public Ayudantia findById(Ayudantia ayudantia){
		return ayudantiaDao.findById(ayudantia);
	}

	/*Funcion guardar:
	 * Recibe una ayudantia y se la pasa al Dao para que la guarde en los registros.
	 * 
	 * */
	public Ayudantia guardar(Ayudantia ayudantia){
		return ayudantiaDao.agregar(ayudantia);
	}
	
	
	/**REVISAR SU USO EN EL PROGRAMA
	 * 
	 *
	 * */
	
	public Ayudantia findFirst(){
		return ayudantiaDao.findFirst();
	}

}
