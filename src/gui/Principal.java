package gui;

import java.io.File;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;

import utils.Mail;
import utils.ManagerSession;

import cl.ufro.bd.Asignatura;
import cl.ufro.bd.Ayudantia;
import cl.ufro.bd.Muro;
import cl.ufro.bd.Usuario;

import cl.ufro.controller.AyudantiaController;
import cl.ufro.controller.MuroController;
import cl.ufro.service.AsignaturaService;

import cl.ufro.service.MuroService;

import cl.ufro.controller.UsuarioController;



import cl.ufro.solicitud.ActualizarAyudantia;
import cl.ufro.solicitud.ActualizarMuro;
import cl.ufro.solicitud.ActualizarUsuario;
import cl.ufro.solicitud.IdentificarAyudantia;

import cl.ufro.solicitud.IdentificarUsuario;
import cl.ufro.solicitud.ListarAsignatura;
import cl.ufro.solicitud.ListarMuro;
import cl.ufro.solicitud.RegistrarAyudantia;
import cl.ufro.solicitud.RegistrarUsuario;


public class Principal {

	public static void main(String[] args) {
		System.out.println("::SERVIDOR::");
		System.out.println("::Corriendo en el puerto 5000::");
		/*Establecemos una conexion mediante el puerto 5000
		 * */
		try{
			ServerSocket serverSocket = new ServerSocket(5000);
			while(true){
				Socket socket = serverSocket.accept();
				ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
				Object object = ois.readObject();

				/* Registramos la ayudantia, el id es incremental para evitar conflictos con id repetidos
				 * */
				/*
				if(object instanceof IdentificarMuro){
					IdentificarMuro identificarMuro = (IdentificarMuro)object;
					Muro m= new Muro();
					m.setId(identificarMuro.getId());
					if(identificarMuro.getMensaje()==null){
						m.setMensaje("");
					}
					m.setMensaje(identificarMuro.getMensaje());
					MuroController muroController = new MuroController();
					m = muroController.identificar(m); //Muro que envio devuelta a mi cliente
					ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
					oos.writeObject(m);

				}
				 */
				//if(object instanceof ServicioNotificacion){
					
				
				if(object instanceof ActualizarMuro){
					ActualizarMuro actualizarMuro = (ActualizarMuro)object;
					Muro m = new Muro();
					m.setCorreo(actualizarMuro.getCorreo());
					m.setIdAyudantia(actualizarMuro.getIdAyudantia());
					m.setMensaje(actualizarMuro.getMensaje());
					MuroController muroController = new MuroController();
					m= muroController.guardar(m);
					
					ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
					oos.writeObject(m);
				}
				if(object instanceof ListarMuro){
					ListarMuro listarMuro = (ListarMuro)object;
					MuroService muroService = new MuroService();
					Ayudantia a = new Ayudantia();
					a.setId(listarMuro.getIdAyudantia());
					List<Muro> lista = muroService.listar(a);
					ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
					oos.writeObject(lista);	
				}

				if(object instanceof ListarAsignatura){
					//Falta llamar al controller
					AsignaturaService asignaturaService = new AsignaturaService();
					List<Asignatura> lista = asignaturaService.listar();
					ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
					oos.writeObject(lista);
				}
				if(object instanceof ActualizarAyudantia){
					ActualizarAyudantia actualizarAyudantia = (ActualizarAyudantia)object;
					Ayudantia ayudantia = new Ayudantia();
					ayudantia.setAyudante(actualizarAyudantia.getAyudante());
					ayudantia.setConfirmar(actualizarAyudantia.isConfirmar());
					ayudantia.setContenidos(actualizarAyudantia.getContenidos());
					ayudantia.setCorreoUsuario(actualizarAyudantia.getCorreoUsuario());
					ayudantia.setFecha(actualizarAyudantia.getFecha());
					ayudantia.setId(actualizarAyudantia.getId());
					ayudantia.setMateria(actualizarAyudantia.getMateria());
					ayudantia.setnPersonas(actualizarAyudantia.getnPersonas());
					ayudantia.setOferta(actualizarAyudantia.getOferta());
					ayudantia.setIdLastMsj(actualizarAyudantia.getIdLastMsj());
					AyudantiaController ayudantiaController = new AyudantiaController();
					ayudantiaController.actualizar(ayudantia);
					ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
					oos.writeObject(ayudantia);

				}
				if(object instanceof IdentificarAyudantia){
					IdentificarAyudantia identificarAyudantia = (IdentificarAyudantia)object;
					Ayudantia ayudantia = new Ayudantia();
					ayudantia.setId(identificarAyudantia.getId());
					ayudantia.setFecha(identificarAyudantia.getFecha());
					ayudantia.setAyudante(identificarAyudantia.getAyudante());
					ayudantia.setConfirmar(identificarAyudantia.isConfirmar());
					ayudantia.setContenidos(identificarAyudantia.getContenidos());
					ayudantia.setCorreoUsuario(identificarAyudantia.getCorreoUsuario());
					ayudantia.setMateria(identificarAyudantia.getMateria());
					ayudantia.setnPersonas(identificarAyudantia.getnPersonas());
					ayudantia.setOferta(identificarAyudantia.getOferta());
					ayudantia.setIdLastMsj(identificarAyudantia.getIdLastMsj());
					AyudantiaController ayudantiaController = new AyudantiaController();
					ayudantia = ayudantiaController.findById(ayudantia); //Ayudantia que envio devuelta a mi cliente
					ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
					oos.writeObject(ayudantia);
				}
				if(object instanceof RegistrarAyudantia){
					RegistrarAyudantia registrarAyudantia = (RegistrarAyudantia)object;
					Ayudantia ayudantia1 = new Ayudantia(); // Llamamos una ayudantia para poder guardar los datos que nos envia el client	
					AyudantiaController ayudantiaController = new AyudantiaController();

					ayudantia1.setContenidos(registrarAyudantia.getContenidos()); 
					ayudantia1.setCorreoUsuario(registrarAyudantia.getCorreoUsuario());
					ayudantia1.setFecha(registrarAyudantia.getFecha());
					ayudantia1.setMateria(registrarAyudantia.getMateria());
					ayudantia1.setnPersonas(registrarAyudantia.getnPersonas());
					ayudantia1.setOferta(registrarAyudantia.getOferta());
					ayudantia1.setIdLastMsj(registrarAyudantia.getIdLastMsj());
					ayudantia1= ayudantiaController.guardar(ayudantia1); // Se guarda en el archivo
					System.out.println("Ayudantia creada con exito");
					ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
					oos.writeObject(ayudantia1);


				}
				if(object instanceof IdentificarUsuario){
					IdentificarUsuario identificarUsuario = (IdentificarUsuario)object;
					Usuario usuario= new Usuario();
					usuario.setCorreo(identificarUsuario.getCorreo());
					usuario.setPin(identificarUsuario.getPin());
					UsuarioController usuarioController = new UsuarioController();
					usuario = usuarioController.identificar(usuario); //usuario que envio devuelta a mi cliente
					ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
					oos.writeObject(usuario);
				}
				/*  Si se instancia un objeto actualizar usuario
				 *  seteamos los campos y llamamos al servicio para llegar
				 *  al dao y usar update.
				 */
				if(object instanceof ActualizarUsuario){
					ActualizarUsuario actualizarUsuario = (ActualizarUsuario)object;
					Usuario usuario = new Usuario();
					usuario.setCorreo(actualizarUsuario.getCorreo());
					usuario.setNombre(actualizarUsuario.getNombre());
					usuario.setPin(actualizarUsuario.getPin());
					UsuarioController usuarioController = new UsuarioController();
					usuarioController.actualizar(usuario);
					ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
					oos.writeObject(usuario);
				}

				if (object instanceof RegistrarUsuario){
					RegistrarUsuario registrarUsuario = (RegistrarUsuario)object;
					Usuario usuario = new Usuario();
					usuario.setCorreo(registrarUsuario.getCorreo());

					UsuarioController usuarioController = new UsuarioController();
					Usuario userBd1 = usuarioController.findByCorreo(usuario);

					int pin;
					if (userBd1==null){
						pin= (int)(Math.random()*900000)+100000;
						usuario.setPin(pin+"");
						usuarioController.guardar(usuario);
					}else{
						pin=Integer.parseInt(userBd1.getPin()); //se renvia
					}

					String asunto="Registro de usuario UFROAYUDAS";
					String mensaje= "Bienvenido a UFROAYUDAS tu pin de confirmacion es: "+pin;
					System.out.println(mensaje);
					String destinos[]={usuario.getCorreo()};

					boolean isSend = Mail.sendMail(asunto, mensaje, destinos);
					if(isSend)
						System.out.println("Mensaje enviado");
					else
						System.out.println("Mensaje no enviado");
				}

			}

		}catch(Exception e) {e.printStackTrace();}


	}

}
